# syntax=docker/dockerfile:1
FROM python:3.8

COPY HoeWarmIsHetInDelft.py /app/HoeWarmIsHetInDelft.py
WORKDIR /app

RUN pip install requests

ENTRYPOINT [ "python", "./HoeWarmIsHetInDelft.py" ]
