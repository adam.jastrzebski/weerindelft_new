import requests
import time

uri = 'https://weerindelft.nl/clientraw.txt?' + str(int(time.time()))  # example uri https://weerindelft.nl/clientraw.txt?1646658438032

response = requests.get(uri)
response_tab = response.text.split()

print( int(float(response_tab[4])) )    # example response 12345 0.0 0.0 99 5.7 94 ... ; 5.7 being temperature

